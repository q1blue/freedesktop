<!--
    To fight off spammers, we unfortunately had to reduce the default privileges for new users...

    We are very sorry about it, and we would really like you to come join us, so please
    use this template to request more privileges (project creation/forking) and
    pinky swear that you are not a spammer :D

    Thanks!
-->

Hi,

I can't create new projects or fork an existing one. I am not a spammer and I want to contribute, so please add me to the list of internal users!

**My pledge**:

I promise that:

 - [ ] I will abide by Freedesktop's [code of conduct](https://www.freedesktop.org/wiki/CodeOfConduct/)
 - [ ] I am not a [spammer](https://www.merriam-webster.com/dictionary/spam)
 - [ ] I can see `"external":true` at this [URL](https://gitlab.freedesktop.org/api/v4/user/) (sorry about this, we'll automate this step soon)

**Why am I applying?**:

<!-- Can you summarize in one sentence why you are requesting access?

For example, if you want to add new features to an existing project, you say:

 > I am a libinput user and I would like to work on allowing the user to select between multiple scroll methods.

If you want to fix an issue in an existing project (thanks, you **rock**!), write something like this:

 > I have this rendering issue in this game on this driver, and I would like to try fixing it.

Finally, if you would like to work on a new project you consider being under the freedesktop.org umbrella and would later like to become part of an official project or a new official project:

 > I would like to start working on this new gpu driver which may eventually be merged in Linux or Mesa, so I would like the community to grow in freedesktop.org.

-->

/label ~"Account verification"
